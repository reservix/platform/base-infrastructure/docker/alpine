ARG VERSION=latest
FROM alpine:$VERSION

RUN apk --no-cache add \
    bash \
    coreutils \
    curl \
    git \
    grep \
    jq \
    libcap \
    openssh \
    openssl \
    unzip \
    vault \
    zip

RUN setcap cap_ipc_lock=+ep /usr/sbin/vault

RUN mkdir -p /root/bin

ENV PATH="/root/bin:$PATH"
ENV USER=root
